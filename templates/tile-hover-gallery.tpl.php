<?php
/**
 * Theme template with gallery template
 */
?>
<div class="image_grid portfolio_4col">
  <ul id="list" class="portfolio_list da-thumbs">
    <?php print $content; ?>
  </ul>
</div>