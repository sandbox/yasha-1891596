<?php
/**
 * Theme template with tile template
 */
?>
<li>
  <img src="<?php print $path_thumb; ?>" alt="<?php print $img_alt; ?>">
  <article class="da-animate da-slideFromRight" style="display: block;">
    <h3><?php print $title; ?></h3>
    <em><?php print $subtitle; ?></em>
    <span class="link_post"><a href="<?php print $url; ?>" target="_blank"></a></span>
    <span class="zoom"><a href="<?php print $path_original; ?>" rel="lightbox" title="<?php print $caption; ?>"></a></span>
  </article>
</li>